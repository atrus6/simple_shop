import os

from typing import Set, List

from flask_login import UserMixin, current_user
from app import db, login_manager, app

from sqlalchemy import Column, Table, ForeignKey, event, DDL
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship

from flask_admin.contrib.sqla import ModelView
from flask import Markup
import datetime

@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(email=user_id).first()

class User(UserMixin, db.Model):
    __tablename__ = "user"

    email = db.Column(db.String, primary_key=True)
    password = db.Column(db.String)

    get_newsletter = db.Column(db.Boolean, default=False)

    def get_id(self):
        return self.email
    
product_category_association_table = db.Table(
    "product_category_association_table",
    db.Column("product_id", ForeignKey("products.stripe_id"), primary_key=True),
    db.Column("category_id", ForeignKey("categories.name"), primary_key=True)
)

class Product(db.Model):
    __tablename__ = "products"

    stripe_id: Mapped[str] = mapped_column(primary_key=True)
    name = db.Column(db.String)
    sku = db.Column(db.String)
    description = db.Column(db.String)
    price = db.Column(db.Integer)
    categories: Mapped[List["Category"]] = relationship(secondary=product_category_association_table, back_populates="products")
    images: Mapped[List["Images"]] = relationship()

    def __repr__(self) -> str:
        return self.name
    
    def pretty_price(self) -> str:
        return "$" + '{:,.2f}'.format(self.price/100.0)


class Category(db.Model):
    __tablename__ = "categories"

    name: Mapped[str] = mapped_column(primary_key=True)
    products: Mapped[List["Product"]] = relationship(secondary=product_category_association_table, back_populates="categories")

    def __repr__(self) -> str:
        return self.name

class Images(db.Model):
    __tablename__ = "images"

    url: Mapped[str] = mapped_column(primary_key=True)
    thumb: Mapped[str] = mapped_column(primary_key=True)
    product_id: Mapped[int] = mapped_column(ForeignKey("products.stripe_id"),primary_key=True)

    def __repr__(self) -> str:
        return self.url
    
    @staticmethod
    def create_thumb_url(p:str) -> str:
        prefix = os.path.dirname(p)
        postfix = os.path.basename(p)
        return prefix + "/thumbs/" + postfix
    
class BlogPost(db.Model):
    __tablename__ = "blog_post"

    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str]
    date: Mapped[datetime.date]
    contents = db.Column(db.Text)
    slug: Mapped[str]

    def __repr__(self) -> str:
        return self.title
    
class Order(db.Model):
    __tablename__ = "orders"

    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    stripe_id: Mapped[str]
    fulfilled: Mapped[bool]

class MyModelView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated and current_user.email in app.config["ADMIN_EMAILS"]

class UserAdmin(MyModelView):
    column_display_pk = True

def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()

    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance
