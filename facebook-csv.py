import os
import sys
import csv

from nocodb.infra.requests_client import NocoDBRequestsClient
from nocodb.nocodb import NocoDBProject, APIToken
from nocodb.filters import InFilter, EqFilter

from product import Product

from dotenv import load_dotenv

load_dotenv()

client = NocoDBRequestsClient(
    APIToken(os.environ["NOCODB"]), "http://home.timchi.me:25242"
)

project = NocoDBProject("noco", "Products")

table_rows = client.table_row_list(
    project, "Catnipporium", InFilter("SKU", "3-"), params={"limit": 1000}
)
facebook_header = [
    "id",
    "title",
    "description",
    "availability",
    "condition",
    "price",
    "link",
    "image_link",
    "brand",
    "google_product_category",
    "fb_product_category",
    "quantity_to_sell_on_facebook",
    "sale_price",
    "sale_price_effective_date",
    "item_group_id",
    "gender",
    "color",
    "size",
    "age_group",
    "material",
    "pattern",
    "shipping",
    "shipping_weight",
    "decor_style",
    "product_length",
    "product_width",
    "product_height",
    "product_weight",
    "number_of_drawers",
    "number_of_shelves",
    "finish",
    "scent",
    "capacity",
    "shape",
    "is_assembly_required",
    "is_powered",
    "power_type",
    "theme",
    "occasion",
    "character",
    "mount_type",
    "recommended_rooms",
    "additional_features",
    "standard_features",
    "numberof_lights",
    "light_bulb_type",
]
lines = []

for row in table_rows["list"]:
    if row["Active"] != True:
        continue

    csvdict = {}
    csvdict["id"] = row["SKU"]
    csvdict["title"] = row["Name"]
    csvdict["description"] = row["Description"]
    csvdict["availability"] = "in stock"
    csvdict["condition"] = "new"
    csvdict["price"] = str(row["Regular price"]) + " USD"
    csvdict["link"] = 'https://catnipporium.com/product/' + row["SKU"]
    csvdict["image_link"] = row['Image1']
    csvdict["brand"] = "Catnipporium"
    csvdict["google_product_category"] = "Animals & Pet Supplies > Pet Supplies > Cat Supplies > Cat Toys"
    csvdict["fb_product_category"] = "pet supplies > cat supplies"
    csvdict["quantity_to_sell_on_facebook"] = "1000"
    csvdict["gender"] = "unisex",
    csvdict["age_group"] = "all ages"
    csvdict["shipping_weight"] = str(row["Weight"]) + "oz"

    lines.append(csvdict)

with open('fb.csv', 'w', newline='') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=facebook_header)

    writer.writeheader()
    writer.writerows(lines)

    
