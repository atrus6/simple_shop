import csv
import config

def append_image(append, img):
    if img != "":
        return append + "," + img
    else:
        return append

def write_google_sheet():
    filename = 'products.csv'
    google_headers = [
        "id",
        "title",
        "description",
        "availability",
        "link",
        "image_link",
        "price",
        "identifier_exists",
        "brand",
        "additional_image_link",
        "multipack",
        "is_bundle",
        "sell_on_google_quantity"
    ]
    rows = []
    with open(filename) as csvfile:
        products = csv.DictReader(csvfile)
        
        for row in products:
            i2 = row["image2"]
            i3 = row["image3"]
            i4 = row["image4"]
            i5 = row["image5"]
            i6 = row["image6"]

            additional_images = ""
            additional_images = append_image(additional_images, i2)
            additional_images = append_image(additional_images, i3)
            additional_images = append_image(additional_images, i4)
            additional_images = append_image(additional_images, i5)
            additional_images = append_image(additional_images, i6)


            d = {
                "id": row["sku"],
                "title": row["name"],
                "description": row["description"],
                "availability": "in_stock",
                "link": config.base_url + "/product/" + row["sku"],
                "image_link": row["image1"],
                "price": '{:,.2f}'.format(int(row["regular_price"])/100.0) + " USD",
                "identifier_exists": "no",
                "brand": config.metadata_business,
                "additional_image_link": additional_images,
                "sell_on_google_quantity": row['inventory']
            }

            rows.append(d)

    with open("google.txt", 'w') as op:
        writer = csv.DictWriter(op, google_headers, delimiter='\t')
        writer.writeheader()
        writer.writerows(rows)