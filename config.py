"""Business Name"""
metadata_business = "Simple Shop"

"""Business Address"""
street1 = "123 Howard Lane"
street2 = "Apt 2"
city = "Anywhere"
state = "AK"
postal_code = "99999"
country = "US"

"""Base URL, no ending /"""
base_url = "https://example.org"


