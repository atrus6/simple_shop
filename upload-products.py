import os
from os.path import isfile,join
import sys
import csv
from datetime import datetime

from typing import List

import stripe

from tqdm import tqdm

from app import db,app
from models import get_or_create, Category, BlogPost, Product, Images

from markdown2 import markdown
import frontmatter
import random
import string
import config

from dotenv import load_dotenv

load_dotenv()
import config

def upload_blog_posts():
    with app.app_context():
        bp_path = "blog_posts"
        
        for f in tqdm(os.listdir(bp_path)):
            if isfile(join(bp_path, f)):
                with open(join(bp_path, f)) as post:
                    post = frontmatter.loads(post.read())
                    slug = post["slug"] + "_" + str(post["id"])
                    get_or_create(db.session, BlogPost, id=post["id"], title=post["title"], date=post["date"], contents=post.content, slug=slug)

def create_google_tsv():
    from google import write_google_sheet
    write_google_sheet()

def upload_to_stripe():
    with open("products.csv") as csvfile:
        if app.debug:
            sid = "stripe_id_test"
            print('Syncing Test Products')
        else:
            sid = "stripe_id"
            test_mode = False
            print('Syncing Live Products')
        
        table_rows = csv.DictReader(csvfile)
        update_csv = False
        rows = []

        for row in tqdm(table_rows):
            metadata = {"business": config.metadata_business, "sku": row["sku"]}
            default_price = {"currency": "USD", "unit_amount": row["regular_price"]}

            # Here we are cleaning up the URLs to point to our server.
            np = []

            for ind in range(1,7):
                column_name = 'image' + str(ind)

                if row[column_name] != '' and row[column_name] != None:
                    np.append(row[column_name])
            
            
            # Here we are either adding nonexistant products, or updating products on Stripes end.
            if row[sid] in (None, ""):
                res = stripe.Product.create(name=row["name"], default_price_data=default_price, metadata=metadata, description=row['description'], images=np, shippable=True)
                row_info = {sid: res['id']}
                row[sid] = res['id']
                update_csv = True
            else:
                pr = stripe.Product.retrieve(row[sid])

                needs_refresh = False

                if pr["description"] != row["description"]:
                    needs_refresh = True
                    print(pr["description"], row["description"])
                    print("Description needs updated for ", row["sku"])
                if pr["metadata"] != metadata:
                    needs_refresh = True
                    print(pr["metadata"], metadata)
                    print("Meta data needs updated for", row["sku"])
                if pr["images"] != np:
                    print(pr["images"], np)
                    needs_refresh = True
                    print("Images need refreshed for ", row["sku"])
                if pr["name"] != row["name"]:
                    needs_refresh = True
                    print(pr["name"], row["name"])
                    print("Name needs refreshed for ", row["name"])

                if needs_refresh:
                    stripe.Product.modify(pr["id"], images=np, metadata=metadata, description=row['description'], name=row['name'])

            rows.append(row)
            # here we update or create new products
            with app.app_context():
                categories = row["categories"].lower().split(',')
                dbcat:List[Category] = []

                for category in categories:
                    dbcat.append(get_or_create(db.session, Category, name=category))

                product = get_or_create(db.session, Product, stripe_id=row[sid])

                for image in np:
                    img = get_or_create(db.session, Images, url=image, thumb=Images.create_thumb_url(image), product_id=product.stripe_id)

                product.name = row["name"]
                product.sku = row["sku"]
                product.description = row["description"]
                product.price = row["regular_price"]
                product.categories = dbcat

                db.session.commit()

        if update_csv:
            with open("products.csv", 'w') as op:
                writer = csv.DictWriter(op, table_rows.fieldnames, lineterminator = '\n')
                writer.writeheader()
                writer.writerows(rows)

def sync_everything():
    print("Uploading Blog Posts")
    upload_blog_posts()
    print("Uploading Products")
    upload_to_stripe()
    print("Writing Google TSV")
    create_google_tsv()
    print('Sync Complete')



        

