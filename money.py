from flask import Blueprint, request, jsonify, render_template

from models import User, Order, get_or_create

import os

from app import app, db, email
from shop import stripe

import easypost
from flask_login import current_user
import config

money = Blueprint("money", __name__)

if app.debug:
    endpoint_secret = os.environ["STRIPE_TEST_ENDPOINT"]
    easypost_key = os.environ["EASYPOST_TEST_KEY"]
else:
    endpoint_secret = os.environ["STRIPE_LIVE_ENDPOINT"]
    easypost_key = os.environ["EASYPOST_LIVE_KEY"]

@money.route("/stripe_webhook", methods=["POST"])
def stripe_webhook():
    from shop import stripe
    print(endpoint_secret)
    payload = request.data
    sig_header = request.headers["STRIPE_SIGNATURE"]

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        raise e
    except stripe.error.SignatureVerificationError as e:
        raise e
    
    if event['type'] == 'checkout.session.completed':
        get_or_create(db.session, Order, stripe_id=event['data']['object']['id'], fulfilled=False)
        email.send(
            subject=config.metadata_business + " Thanks You!",
            receivers=event["data"]["object"]["customer_details"]["email"],
            html_template="email/thanks.html",
            body_params= {
                "NAME": "Thank you for your order",
            }
        )
    return jsonify(success=True)

@money.route('/orders')
def orders():
    orders = Order.query.filter_by(fulfilled=False)
    return render_template("auth/orders.html", orders=orders)

@money.route('/order/<id>')
def order(id):
    order = Order.query.filter_by(id=id).first()
    session = stripe.checkout.Session.retrieve(order.stripe_id)
    cust_email = session["customer_details"]["email"]
    address = session['shipping']['address']
    name = session['shipping']['name']
    line_items = stripe.checkout.Session.list_line_items(order.stripe_id)

    oli = []
    for line_item in line_items:
        rv = {}
        rv['quantity'] = line_item['quantity']
        product = stripe.Product.retrieve(line_item['price']['product'])
        rv['name']=product['name']
        if "sku" in product['metadata']:
            rv['metadata'] = product['metadata']['sku']
        else:
            rv['metadata'] = product['metadata']
        oli.append(rv)
    return render_template("auth/order.html", orders=oli, name=name, address=address, id=id, email=cust_email)

@money.route('/ship/<id>')
def ship(id):
    order = Order.query.filter_by(id=id).first()
    session = stripe.checkout.Session.retrieve(order.stripe_id)
    cust_email = session["customer_details"]["email"]
    address = session['shipping']['address']
    address["name"] = session['shipping']['name']
    address["street1"] = address["line1"]
    address["street2"] = address["line2"]
    address["zip"] = address["postal_code"]
    weight = request.args.get('oz')
    length = request.args.get('length')
    width = request.args.get('width')
    height = request.args.get('height')

    from_address = {
        "name": config.metadata_business,
        "street1": config.street1,
        "street2": config.street2,
        "city": config.city,
        "state": config.state,
        "zip": config.postal_code,
        "country": config.country,
    }

    parcel = {
            "length": length,
            "width": width,
            "height": height,
            "weight":weight,
        }

    client = easypost.EasyPostClient(easypost_key)
    shipment = client.shipment.create(
        from_address=from_address,
        to_address=address,
        parcel= parcel,
    )
    
    bought_shipment = client.shipment.buy(shipment.id, rate=shipment.lowest_rate())
    client.shipment.label(bought_shipment.id, file_format="png")
    order.fulfilled = True
    db.session.commit()
    tracker = bought_shipment["tracker"]["public_url"]
    email.send(
        subject=metadata_business + "Thanks You!",
        receivers=cust_email,
        html_template="email/shipped.html",
        body_params = {
            "NAME": "Thank you!",
            "tracking_link": tracker
        },
    )
    return "<img src='" + bought_shipment["postage_label"]["label_url"] + "'>"
