import os, secrets
from typing import List

from urllib.parse import urlencode

from flask import render_template, request, make_response, Blueprint, flash, redirect, url_for, abort, session, request
from flask_login import login_user, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash

import requests

from app import app, db
from models import User, Product, Category, Images, get_or_create
from shop import stripe

auth = Blueprint("auth", __name__)

app.config["OAUTH2_PROVIDERS"] = {
    'google': {
        'client_id': os.environ.get('GOOGLE_CLIENT_ID'),
        'client_secret': os.environ.get('GOOGLE_CLIENT_SECRET'),
        'authorize_url': 'https://accounts.google.com/o/oauth2/auth',
        'token_url': 'https://accounts.google.com/o/oauth2/token',
        'userinfo': {
            'url': 'https://www.googleapis.com/oauth2/v3/userinfo',
            'email': lambda json: json['email'],
        },
        'scopes': ['https://www.googleapis.com/auth/userinfo.email'],
    },
    'facebook': {
        'client_id': os.environ.get('FACEBOOK_CLIENT_ID'),
        'client_secret': os.environ.get('FACEBOOK_CLIENT_SECRET'),
        'authorize_url': "https://www.facebook.com/v19.0/dialog/oauth",
        "token_url": "https://graph.facebook.com/v19.0/oauth/access_token",
        "userinfo": {
            "url": "https://graph.facebook.com/v19.0/me?fields=email",
            "email": lambda json: json['email'],
        },
        "scopes": ["email"],
    }
}

@auth.route("/login", methods=["get", "post"])
def login():
    if request.method == "GET":
        return render_template("login.html")
    elif request.method == "POST":
        email = request.form.get('email')
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False

        user = User.query.filter_by(email=email).first()

        if not user or not check_password_hash(user.password, password):
            flash('Please check your login details and try again.')
            return redirect(url_for('auth.login'))
        
        login_user(user, remember=remember)
        return redirect(url_for('auth.profile'))

@auth.route("/signup", methods=["GET", "POST"])
def create_user():
    if request.method == "GET":
        return render_template("signup.html")
    elif request.method == "POST":
        data = request.form.to_dict()
        em = data["email"]
        password = data["password"]
        remember = True if request.form.get('remember') else False

        old_user = User.query.filter_by(email=em).first()
        if old_user:
            flash('Email address already exists')
            return redirect(url_for('auth.login'))

        new_user = User(email=em, password=generate_password_hash(password, method='pbkdf2'))

        db.session.add(new_user)
        db.session.commit()

        login_user(new_user, remember=remember)
        return redirect(url_for('auth.profile'))

@auth.route("/logout")
def logout():
    logout_user()
    flash("You have been logged out.")
    return redirect(url_for("auth.login"))

@auth.route("/authorize/<provider>")
def oauth2_authorize(provider):
    if not current_user.is_anonymous:
        return redirect(url_for("auth.login"))
    
    provider_data = app.config["OAUTH2_PROVIDERS"].get(provider)
    if provider_data is None:
        abort(404)

    session["oauth2_state"] = secrets.token_urlsafe(16)

    qs = urlencode({
        "client_id": provider_data["client_id"],
        "redirect_uri": url_for("auth.oauth2_callback", provider=provider, _external=True, _scheme='https'),
        "response_type": "code",
        "scope": " ".join(provider_data["scopes"]),
        "state": session["oauth2_state"]
    })
    print(qs)

    return redirect(provider_data["authorize_url"] + "?" + qs)

@auth.route("/callback/<provider>")
def oauth2_callback(provider):
    if not current_user.is_anonymous:
        return redirect(url_for("auth.login"))
    
    provider_data = app.config["OAUTH2_PROVIDERS"].get(provider)
    if provider_data is None:
        abort(404)

    if 'error' in request.args:
        for k, v in request.args.items():
            if k.startswith("error"):
                flash(f'{k}: {v}')
        return redirect(url_for("auth.login"))
    
    if request.args['state'] != session.get("oauth2_state"):
        abort(401)

    if "code" not in request.args:
        abort(401)

    response = requests.post(provider_data["token_url"], data={
        "client_id": provider_data["client_id"],
        "client_secret": provider_data["client_secret"],
        "code": request.args["code"],
        "grant_type": "authorization_code",
        "redirect_uri": url_for("auth.oauth2_callback", provider=provider, _external=True, _scheme='https'),
    }, headers={"Accept": "application/json"})

    if response.status_code != 200:
        abort(401)

    oauth2_token = response.json().get("access_token")

    if not oauth2_token:
        abort(401)

    response = requests.get(provider_data['userinfo']['url'], headers={
        "Authorization": "Bearer " + oauth2_token,
        "Accept": "application/json",
    })

    if response.status_code != 200:
        print("RESPONSE ", response)
        abort(401)

    email = provider_data["userinfo"]["email"](response.json())

    user = get_or_create(db.session, User, email=email)

    login_user(user)

    return redirect(url_for("shop.index"))

@auth.route("/profile")
def profile():
    return redirect(url_for('shop.index'))

@auth.route("/admin/refresh_products")
def refresh_products():
    if current_user.is_authenticated and current_user.email in app.config["ADMIN_EMAILS"]:
        import importlib
        up = importlib.import_module("upload-products")
        up.sync_everything()
    return redirect(url_for('shop.index'))